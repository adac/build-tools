(import json)


# utils

(defn map-values
  `Map f over the values of a dictionary.`
  [f d]
  (def res @{})
  (loop [[k v] :pairs d]
    (put res k (f v)))
  res)

(defn update-value
  `Does the same as update without adding a missing key.`
  [ds key func & args]
  (if (ds key) (apply update ds key func args) ds))

(defn unless-is-array-wrap-in-array
  `Wrap the form in an array if it isn't one already.`
  [form] (if (indexed? form) form @[form]))


# work's being done here

(defn update-content [content]
  (when content
    (-> content
	(update-value "Budget" unless-is-array-wrap-in-array))))

(defn update-entry [entries]
  (map-values
   (fn [entry]
       (update entry "content" update-content))
   entries))


# entrypoint of executable

(defn main [& args]
  (map
   (fn [file]
     (->> (slurp file)
	  (json/decode)
	  (update-entry)
	  (json/encode)
	  (spit file)))
   (slice args 1)))
