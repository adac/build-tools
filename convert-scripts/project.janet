(declare-project
  :name "convert-rules"
  :author "Adriel Dumas--Jondeau"
  :license "Custom"
  :url "https://codeberg.org:adac/build-tools"
  :repo "git@codeberg.org:adac/build-tools"
  :dependencies ["https://github.com/janet-lang/json"])

(declare-executable
  :name "convert"
  :entry "convert.janet"
  :install false)
