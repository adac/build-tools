param ( [string]$AppPath, [string]$UserFolder )

$DesktopPath = [Environment]::GetFolderPath(
    [System.Environment+SpecialFolder]::Desktop
)
$LinkPath = Join-Path -Path $DesktopPath -ChildPath "Saisie.lnk"
$TargetPath = Join-Path -Path $AppPath -ChildPath "saisie-server.exe"
$OutputPath = Join-Path -Path $UserFolder -ChildPath "db.json"

# Create and configure shortcut
$ShellLink = New-Object -comObject WScript.Shell
$Shortcut = $ShellLink.CreateShortcut($LinkPath)
$Shortcut.TargetPath = $TargetPath
$Shortcut.Description = "Logiciel de saisie pour les missions de l’ADAC."
$Shortcut.WorkingDirectory = $AppPath
$Shortcut.Arguments = "--output `"$OutputPath`""

# Commit
$Shortcut.Save()
